package learn_Object;

public class TypeCastingDemo {

	public static void main(String[] args) {
		
           int i = 10; // widen casting
           long j = i;
           
           int ii = (int)j; // narrow casting

	}

}
