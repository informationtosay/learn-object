package learn_Object;

public class Employee extends Student
{

	public static void main(String[] args) {
		
		Employee emp = new Employee();
		
		Student s = emp; // Upcasting or widen casting --> child to parent
		emp.study();
	//	s.doProject(); --> error come can not access this line
		
		Employee emp2 = (Employee)s; // Downcasting or narrow casting --> parent to child
		
		emp.doProject();

	}

	public void doProject() {
		System.out.println("project");
		
	}

}
