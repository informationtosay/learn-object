package learn_Object;

public class Object_Tech 
{
	String name;
	public Object_Tech(String name) {
		this.name=name;
	}

	public static void main(String[] args) {
		Object_Tech ot = new Object_Tech("kholi");
		System.out.println(ot);
		
		String s = new String ("dhoni");
		System.out.println(s);
		ot.ram(ot);
		ot.ram(10);
		
		int no2 = 10; // primitive data type to change object
		Integer i2 = no2; // changed to object - auto boxing
		int no3 = i2; // auto unboxing
		
		Integer i4 = new Integer(10); // legacy old type
		ot.ram(i4); // Auto unboxing
		
		
	}
	  public String toString() {// toString method overriding
	        return this.name;
	    }
	  
	  public void ram(Object o) {
		  
	  }
	  public void ram(Integer s) { // auto-boxing
		  System.out.println("integer");
	  }
	  public void ram(int d) {
		  System.out.println("int");
	  }
	  
	 
}
